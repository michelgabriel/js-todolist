document.getElementById("newTask").addEventListener("click", function(){
    createTask();
});

document.getElementById("newTask").addEventListener("onkeypress", function(e){
    if(e.keyCode === 13){
        createTask();
    }
});

function createTask(){
    var task = document.getElementById("task").value;
    if (task === ""){
        alert("Enter a Task!");
        return false;
    }
    var count = document.querySelectorAll('.list-group-item').length;
    for (var i = 0; i < count; i++) {
        var item = document.getElementsByClassName("list-group-item")[i].innerHTML;
        if(item == task){
            alert("Task already exists");
            return false;
        }
    }
    var li = document.createElement('li');
    li.innerHTML = task;
    li.className = "list-group-item";

    var a = document.createElement('a');
    a.id = "deleteTask";
    a.setAttribute("href", "#");
    a.setAttribute("onclick", "deleteTask(this);");

    var i = document.createElement('i');
    i.className = "fa fa-trash-o fa-2x";
    i.setAttribute("aria-hidden", "true");

    var ul = document.getElementById("tasks");

    a.appendChild(i)
    ul.appendChild(li);
    ul.appendChild(a);
}

function deleteTask(e){
    e.previousSibling.remove();
    e.remove();
}